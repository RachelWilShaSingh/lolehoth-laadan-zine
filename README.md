# Lolehoth 

![Screenshot of Lolehoth definition](readme/lolehoth.png)

## About

Lolehoth is a project to compile media in and about Láadan by the community of Láadan enthusiasts and for the community.
The intension is to create a *zine* (indie magazine) that can be distributed online (in a readable AND printable format),
as well as distributing hard copies over mail or in person.

This could also be a good chance to work with others so we can "QA" each others' works, and get better at using Láadan in the practice.

Would you like to contribute anything? You can contact me at **Rachel@Moosader.com**

## How to Contribute

If you are familiar with Source Control and Git, you can contribute directly through GitLab,
but if you're not used to these tools, you can work directly with Rachel!

Contributions can be submissions of your own original works, or providing feedback / corrections on other works.

**Through Git**

* Make a fork of this repository (note that the primary branch is named "nedeya", NOT "master"!)
  After making changes, create a **Pull Request** for review.
  
**Through humans**

* Send an email to **Rachel@moosader.com** to send something in (please put "Lolehoth" in the subject).

### General notes about contributing

**Things you should provide**

* Please make sure to specify how you'd like to be credited, please include your pronouns.
* Any contact information (or links to social media) you'd like included.
* Please submit an English language translation
  (plus any other translations you'd like to include) if the contribution is in Láadan.
* Please specify a license; you might consider using a 
  [Creative Commons](https://creativecommons.org] license in order to provide your content for free,
  but also ensure attribution and any additional restrictions.
* Contributions should be **original works**, **works you own the rights to**, or possibly **translations of Public Domain works**.
  
**Zine use**

* This is intended to be a free resource for people, or possibly to be sold
  for the cost of materials (paper, ink, shipping) if being provided in physical form.
* Each zine will also be available in *readable* and *printable* formats so people can
  read it digitally, or print it out and assemble the zine themselves.
* Works in Láadan (not just *about* Láadan** should include a translation,
  both for community QA checks, as well as to help out novices.
  
**Other notes*

* Any hateful content and bigotry will not be permitted. This includes trans-exclusionary content,
racism, sexism, homomisia (hatred of homosexuality) or other sexualities (including asexuality),
and so on.

### What to contribute?

Some ideas!

* Short stories
* Poems
* Songs (+Sheet music / tabs?)
* Comic strips
* Artwork (Doesn't require Láadan writing; could be given to decorate the zine)
* Articles (or news blurbs or short thoughts) about Láadan, languages, feminism, LGBTQIA+ topics, etc.
* "Ads" for your own works / where to find you!
  * Social media? YouTube? Video games or other media that isn't print-usable?
* Pen-pal/Email-pal requests?
* Puzzles
* Printable activities (e.g., a Láadan calendar)

Basically, anything!


## Contributors - past and present

WIP
